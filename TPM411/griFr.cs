﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPM411
{
    public partial class griFr : Form
    {
        Bitmap kaynak, islem;
        public griFr()
        {
            InitializeComponent();
        }

        private void ortalamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)
            {
                for (int y=0;y<yuk;y++)
                {
                    Color renkliPiksel = kaynak.GetPixel(x, y);
                    int griDeger = (renkliPiksel.R + renkliPiksel.G + renkliPiksel.B) / 3;
                    Color griPiksel = Color.FromArgb(griDeger, griDeger, griDeger);
                    islem.SetPixel(x, y, griPiksel);
                }
            }

            islemBox.Image = islem;
        }

        private void bT709ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)
            {
                for (int y = 0; y < yuk; y++)
                {
                    Color renkliPiksel = kaynak.GetPixel(x, y);
                    double G = (renkliPiksel.R * 0.2125) + (renkliPiksel.G * 0.7154) + (renkliPiksel.B * 0.072);
                    int Gri = Convert.ToInt32(G);
                    Color Piksel = Color.FromArgb(Gri, Gri, Gri);
                    islem.SetPixel(x, y, Piksel);
                }
            }

            islemBox.Image = islem;

        }

        private void lumaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)

            {
                for (int y = 0; y < yuk; y++)
                {
                    Color renklirPiksel = kaynak.GetPixel(x, y);


                    double G = (renklirPiksel.R * 0.3) + (renklirPiksel.G * 0.59) + (renklirPiksel.B * 0.11);
                    int Gri = Convert.ToInt32(G);
                    Color griPiksel = Color.FromArgb(Gri, Gri, Gri);
                    islem.SetPixel(x, y, griPiksel);
                }
            }
            islemBox.Image = islem;
        }

        private void renkKanalıToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)
            {
                for (int y = 0; y < yuk; y++)
                {
                    Color renkliPiksel = kaynak.GetPixel(x, y);
                   
                    Color griPiksel = Color.FromArgb(renkliPiksel.R, renkliPiksel.R, renkliPiksel.R);
                   // Color griPiksel = Color.FromArgb(renkliPiksel.G, renkliPiksel.G, renkliPiksel.G);
                  //  Color griPiksel = Color.FromArgb(renkliPiksel.B, renkliPiksel.B, renkliPiksel.B);
                    islem.SetPixel(x, y, griPiksel);
                }
            }

            islemBox.Image = islem;
        }

        private void normalizeEdilmişToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);
            for (int x = 0; x < gen; x++)
            {
                for (int y = 0; y < yuk; y++)
                {
                    Color renkliPiksel = kaynak.GetPixel(x, y);
                    int T = (renkliPiksel.R + renkliPiksel.G + renkliPiksel.B);
                    if (T > 0) 
                    {
                        //int Ir = (255 * renkliPiksel.R) / T;
                        int Ir = (255 * renkliPiksel.G) / T;
                       // int Ir = (255 * renkliPiksel.B) / T;
                    }

                    Color griPiksel = Color.FromArgb(renkliPiksel.R, renkliPiksel.R, renkliPiksel.R);
                    islem.SetPixel(x, y, griPiksel);
                }
            }
            islemBox.Image = islem;
        }
        private void açToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                kaynak = new Bitmap(openFileDialog1.FileName);
                kaynakBox.Image = kaynak;
            }
        }
    }
}
